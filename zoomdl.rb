require 'json'
require 'pp'
require 'net/http'
require 'date'
require 'open-uri'

def sanitize_filename(filename)
  filename = filename.downcase.strip.gsub(' ', '-').gsub(/[^\w-]/, '')
  return filename
end

def uniqueid()
  return rand(10 ** 10).to_s.rjust(10,'0')
end

def download_video(url, filename)
  puts "Downloading " + filename
  download = open(url)
  IO.copy_stream(download, filename)
end

url = 'https://api.zoom.us/v1/recording/list'
apikey = ''
apisecret = ''
hostid=''

uri = URI(url)
res = Net::HTTP.post_form(uri, 'api_key' => apikey, 'api_secret' => apisecret, 'host_id' => hostid)

zoom = JSON.parse(res.body)

zoom['meetings'].each do |item|
  topic = item['topic']
  event = item['recording_files']
  event_date = Date.rfc3339(event[0]['recording_start']).to_s
  uniq = uniqueid()
  filename = sanitize_filename("#{topic}-#{event_date}-#{uniq}") + '.mp4'
  video_url = event[0]['download_url']
  download_video(video_url, filename)
end



#TODO: put the videos somewhere with this script, then make a UI for this whole thing
